/*    Copyright 2013 Jon Eby
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.hqas.fingwidgets.gfdawidget;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class GFDAWidgetProvider extends AppWidgetProvider {
    private static final String LOG = "com.hqas.fingwidgets.gfdawidget";

    private static UpdateService service = null;

    // Catch broadcasts from the more advice button,
    // and get more advice, and let other broadcasts be handled
    // as they normally would by the AppWidgetProvider.
    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(UpdateService.ADVICEACTION)) {
            int widgetId = intent.getIntExtra(UpdateService.WIDGETID, 0);
            if (service != null) {
                service.getAdvice(context, service.getViews(context),
                        AppWidgetManager.getInstance(context), widgetId);
            }
        } else {
            super.onReceive(context, intent);
        }
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        Log.w(LOG, "onUpdate method called");
        // Get all widget ids
        ComponentName thisWidget = new ComponentName(context, GFDAWidgetProvider.class);
        if (service == null) {
            service = new UpdateService();
        }
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

        // Build the intent to call the service
        Intent intent = new Intent(context.getApplicationContext(), service.getClass());
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, allWidgetIds);

        // Update the widgets via the service
        context.startService(intent);
    }
}
