/*    Copyright 2013 Jon Eby
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.hqas.fingwidgets.gfdawidget;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class AdviceObject implements Serializable {
    private String new_quote_id;

    private String new_advice;

    public void stripTags() {
        // GFDA returns a tag around their naughty words. We don't want to
        // display it, or do anything else with it, so strip it out with the
        // below regex.
        Pattern pattern = Pattern.compile("(?<=^|>)[^><]+?(?=<|$)");
        String adviceResult = new_advice;
        Matcher matcher = pattern.matcher(adviceResult);
        StringBuilder sanitizedResult = new StringBuilder();
        int groupCounter = 1; // Start with the first match from the regex
        while (matcher.find()) {
            sanitizedResult.append(matcher.group(0).toString());
            groupCounter++;
        }
        this.new_advice = sanitizedResult.toString(); // After we regex it,
                                                      // return the resulting
                                                      // string
    }

    public String getId() {
        return new_quote_id;
    }

    public String getAdvice() {
        return new_advice;
    }
}
