/*    Copyright 2013 Jon Eby
 *
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */

package com.hqas.fingwidgets.gfdawidget;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import com.google.gson.Gson;

public class UpdateService extends Service {
    private static final String LOG = "com.hqas.fingwidgets.gfdawidget";

    public static final String ADVICEACTION = "com.hqas.fingwidgets.MOREADVICE";

    public static final String WIDGETID = "widget_id";

    private final String gfdaUrl = "http://www.goodfuckingdesignadvice.com/";

    private final String adviceParam = "#adviceID=";

    private final String refreshUrl = "http://goodfuckingdesignadvice.com/refresh-advice.php";

    private RemoteViews remoteViews;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(LOG, "Called");
        final AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this
                .getApplicationContext());

        int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

        ComponentName thisWidget = new ComponentName(getApplicationContext(),
                GFDAWidgetProvider.class);
        int[] allWidgetIds2 = appWidgetManager.getAppWidgetIds(thisWidget);

        remoteViews = new RemoteViews(this.getApplicationContext().getPackageName(),
                R.layout.widget_layout);

        for (final int widgetId : allWidgetIds) {
            getAdvice(getApplicationContext(), remoteViews, appWidgetManager, widgetId);

        }
        stopSelf();

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void getAdvice(Context context, RemoteViews views, AppWidgetManager manager, int widgetId) {
        AdviceFetcher fetcher = new AdviceFetcher(context, views, manager, widgetId);
        fetcher.execute();
    }

    public RemoteViews getViews(Context context) {
        if (remoteViews != null) {
            return remoteViews;
        } else {
            return new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        }
    }

    private class AdviceFetcher extends AsyncTask<Void, Void, AdviceObject> {
        private Context context;

        private RemoteViews views;

        private AppWidgetManager manager;

        private int widgetId;

        public AdviceFetcher(Context context, RemoteViews remoteViews, AppWidgetManager manager,
                int widgetId) {
            super();
            this.context = context;
            this.views = remoteViews;
            this.manager = manager;
            this.widgetId = widgetId;
        }

        @Override
        protected AdviceObject doInBackground(Void... params) {
            AdviceObject advice = null;

            HttpClient client = new DefaultHttpClient();
            HttpUriRequest request = new HttpGet(refreshUrl);

            try {
                HttpResponse response = client.execute(request);
                BufferedReader reader = new BufferedReader(new InputStreamReader(response
                        .getEntity().getContent()));

                Gson gson = new Gson();
                advice = gson.fromJson(reader, AdviceObject.class);
                advice.stripTags();

            } catch (ClientProtocolException e) {
                Log.e(getClass().getName(), e.getMessage());
            } catch (IOException e) {
                Log.e(getClass().getName(), e.getMessage());
            }

            return advice;
        }

        @Override
        protected void onPostExecute(AdviceObject advice) {
            if (advice != null) {
                String adviceUrl = gfdaUrl + adviceParam + advice.getId();

                Intent urlIntent = new Intent(Intent.ACTION_VIEW);
                urlIntent.setData(Uri.parse(adviceUrl));

                views.setTextViewText(R.id.txt_advice, advice.getAdvice());

                PendingIntent specificAdviceIntent = PendingIntent.getActivity(context, 0,
                        urlIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                views.setOnClickPendingIntent(R.id.txt_advice, specificAdviceIntent);

                urlIntent = new Intent(Intent.ACTION_VIEW);
                urlIntent.setData(Uri.parse(gfdaUrl));

                PendingIntent goodFuckingAdviceIntent = PendingIntent.getActivity(context, 0,
                        urlIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                views.setOnClickPendingIntent(R.id.txt_gfda, goodFuckingAdviceIntent);

                Intent moreAdviceIntent = new Intent(context, GFDAWidgetProvider.class);
                moreAdviceIntent.setAction(ADVICEACTION);
                moreAdviceIntent.putExtra(UpdateService.WIDGETID, widgetId);

                PendingIntent moreAdvicePendingIntent = PendingIntent.getBroadcast(context, 0,
                        moreAdviceIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                views.setOnClickPendingIntent(R.id.txt_moreadvice, moreAdvicePendingIntent);

                manager.updateAppWidget(widgetId, views);
            }
        }

    }
}
